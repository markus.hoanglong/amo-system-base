import React from 'react'

export function Button() {
  return <button>Hello from AIT RC libs</button>
}

export default React.memo(Button)
