import React from 'react'
import { Button as AntdButton } from 'antd'

function Button(props) {
  const { children, ...restProps } = props
  return <AntdButton {...restProps}>{children}</AntdButton>
}

export default React.memo(Button)
