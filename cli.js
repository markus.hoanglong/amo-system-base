#!/usr/bin/env node

const childProcess = require('child_process')
const path = require('path')

const devProcess = childProcess.exec('electron-vite dev', {
  cwd: path.join(process.cwd(), 'node_modules', 'amo-system'),
})
devProcess.stdout.pipe(process.stdout)
devProcess.stdin.pipe(process.stdin)
devProcess.stderr.pipe(process.stderr)
